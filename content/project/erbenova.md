---
title: "Residence ERBENOVA"
date: 2018-12-01
Description: "Residence ERBENOVA"
studio: "EBM Expert"
location: "Praha"
scope: "Rezidence ERBENOVA je rezidenčný projekt s luxusnými bytovými jednotkami navrhnutý ateliétom ADR.
Na pokyn investora som v tomto projekte spracovala kompletný redizajn existujúceho dispozičného riešenia bytových jednotiek. Zadaním bolo vytvoriť luxusné bývanie s otvorenou dispozíciou a využitím výhľadov, ktoré architektonický návrh ponúka."
year: "2018"
directory: ../../files/06_2018_13_Erbenova/
cover: 00.jpg
img: [01.jpg,
02.jpg,
11.jpg,
12.jpg,
13.jpg,
14.jpg,
15.jpg,
16.jpg,
17.jpg
]
---