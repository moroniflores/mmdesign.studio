---
title: "Krc"
date: 2020-05-01
Description: "Residence KRC"
studio: "EBM Expert"
location: "Praha"
scope: 'Rezidence KRC je bytový dom o troch sekciách, ktorý je nevrhnutý na mieste stávajúcej záhrady. Koncept je preto koncipovaný ako "dom v zahrade". 
V tomto projekte som navrhla kompletné dispozičné riešenie všetkých bytových aj nebytových priestorov, domovnej vybavenosti a parkovacieho podlažia. Podieľala som sa na tvorbe záhradných úprav a návrhu fasádnych konštrukcií.
Na projekte som pracovala v štádiu architektonickej štúdie a následne som spracovala kompletnú dokumentáciu pre stavebné povolenie.'
year: "2019"
directory: ../../files/2020_05_bd_krcska_zahrada/
cover: 00.jpg
img: [
01.jpg,
02.jpg,
11.jpg,
12.jpg,
13.jpg,
14.jpg
]
---