---
title: "Rohan"
date: 2018-11-01
Description: "Residence ROHAN"
studio: "EBM Expert"
location: "Praha"
scope: "Rezidence ROHAN je bytový dom o dvoch sekciách na Rohanskom nábreží. Je súčasťou komplexnej výstavby tohoto rýchlo sa rozvíjajúceho územia v Prahe. 
Tento návrh sa snaží vytvoriť modernú a jednoduchú architektúru. Jedná sa o redizajn existujúceho projektu, ktorý bol na tomto mieste navrhnutý.
V tomto projekte som sa podieľala na návrhu architektonického stvárnenia objektu. Navrhla som komplektné dispozičné riešenie bytových a nebytovách priestorov nadzemných podlaží. Zrevidovala som stávajúce podzemné podlažia tj., podzemné parkovanie a technické zázemeie objektu.
Na projekte som pracovala v štádiu architektonickej štúdie a spracovala som dokumentáciu pre stavebné povolenie."
year: "2018"
directory: ../../files/2018-11_Rohan/
cover: 00.jpg
img: [01.jpg,
02.jpg,
11.jpg,
12.jpg,
13.jpg
]
---