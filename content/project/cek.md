---
title: "Cek"
date: 2017-12-01
Description: "Čtvrť Emila Kolbena"
studio: "EBM Expert"
location: "Prague, Czechia"
scope: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer consequat a eros nec suscipit. Proin consequat odio sem, vel egestas tortor rhoncus sed."
year: "2017"
cover: files/2017-12_Kolben_CEK/cover.jpg
img: [../../files/2017-12_Kolben_CEK/EBM_Kolbenova_001.jpg,
../../files/2017-12_Kolben_CEK/EBM_Kolbenova_002.jpg]
draft: true
---