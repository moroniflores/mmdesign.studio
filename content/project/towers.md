---
title: "Towers"
date: 2018-10-01
Description: "Residence TOWERS"
studio: "EBM Expert"
location: "Praha"
scope: "Rezidence TOWERS je rezidenčný projekt nachádzajúci sa v rozvojovom území mesta Prahy.
Na pokyn investora som v tomto projekte spracovala kompletný redizajn existujúceho dispozičného riešenia všetkých bytových jednotiek."
year: "2017"
cover: files/2018-10_Towers/KOLBEN_010011_varB_170919.jpg
img: [../../files/2018-10_Towers/KOLBEN_010015_varB_170919.jpg,
    ../../files/2018-10_Towers/KOLBEN_010011_varC_171030.jpg,
    ../../files/2018-10_Towers/TWRS_01.jpg,
    ../../files/2018-10_Towers/TWRS_04.jpg,
    ../../files/2018-10_Towers/TWRS_07.jpg,
    ../../files/2018-10_Towers/TWRS_11.jpg,
    ../../files/2018-10_Towers/TWRS_12.jpg,
]
---