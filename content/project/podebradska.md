---
title: "Podebradska"
date: 2018-06-01
Description: "Residence PODEBRADSKA"
studio: "EBM Expert"
location: "Prague, Czechia"
scope: "Rezidence PODEBRADSKA je rezidenčný projekt nachádzajúci sa v rozvojovom území mesta Prahy.
Na pokyn investora som v tomto projekte spracovala kompletný redizajn existujúceho dispozičného riešenia všetkých bytových jednotiek."
year: "2017"
cover: files/2018-06_Podebrad/1.jpg
img: [../../files/2018-06_Podebrad/2.jpg,
    ../../files/2018-06_Podebrad/EBM_Pod%C4%9Bbradsk%C3%A1_02_181003.jpg,
    ../../files/2018-06_Podebrad/POD_02.jpg,
    ../../files/2018-06_Podebrad/POD_10.jpg
    ]
---

