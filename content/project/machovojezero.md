---
title: "Machovojezero"
date: 2020-01-01
Description: "Residence MACHAC"
studio: "EBM Expert"
location: "Staré Splavy"
scope: "Rezidence MACHAC je luxusný bytový dom na nábreží Máchovho jazera. V objekte sa nachádza 8 bytových jednotiek s vysokým štadartom vybavenia a materiálov. 
V tomto projekte som navrhla kompletné dispozičné riešenie všetkých bytových aj nebytových priestorov. Podieľala som sa na tnávrhu záhradných úprav, návrhu technického riešenia objektu, koncept vedenia tras inštalácií a koncept riešenia stavebných detailov.
Na projekte som pracovala v štádiu architektonickej štúdie, dokumentácie pre stavebné povolenie a spracovala som marketingovú dokumentáciu projektu."
year: "2019"
directory: ../../files/2019_01_machovojezero/
cover: 00.jpg
img: [
01.jpg,
02.jpg,
11.jpg,
12.jpg,
13.jpg,
14.jpg]
---