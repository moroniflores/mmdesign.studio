---
title: "Mosilana"
date: 2019-05-01
Description: "Urban design MOSILANA"
studio: "EBM Expert"
location: "Brno"
scope: "Urbanistická štúdia MOSILANA je návrh riešenia rozľahlého rozvojového územia, ktoré sa nachádza v tesnej blízkosti historického centra mesta Brno.
Konceptom tejto štúdie je oddelenie automobilovej dopravy a uvoľnenie centrálnej časti lokality, kde vznikne nová mestská trieda s bohatou zeleňou. Návrh počíta s vytvorením zástavby so zmiešanou funkciou tj., bývanie a komercia.
V tomto projekte som sa podieľala na koncepčnom riešení územia, navrhla som dispozičné riešenie bytových a nebytových priestorov jednotlivých typov rezidenčných a komerčných objektov. Spracovala som kompletnú grafickú čásť prezentácie projektu v štádiu urbanistická štúdia."
year: "2019"
directory: ../../files/2019_05_US_Mosilana/
cover: 00.jpg
img: [
01.jpg,
02.jpg,
11.jpg,
12.jpg,
13.jpg,
14.jpg,
15.jpg]
---
