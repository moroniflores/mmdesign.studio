---
title: "Jinonice"
date: 2019-06-01
Description: "Urban design JINONICE"
studio: "EBM Expert"
location: "Praha"
scope: "Urbanistická štúdia JINONICE je návrh riešenia rozľahlého rozvojového územia, ktoré sa viaže na výstavbu budúcej Radlickej radiály.
Konceptom tejto štúdie bolo využiť kvalitu pozemku a jeho polohy a vytvoriť tak typy bývania pre rôzne sociálne vrstvy spoločnosti.
V tomto projekte som sa podieľala na koncepčnom riešení územia, navrhla som dispozičné riešenie bytových a nebytových priestorov všetkých typov rezidenčných objektov. Spracovala som kompletnú grafickú čásť prezentácie projektu v štádiu due diligence a urbanistická štúdia."
year: "2019"
directory: ../../files/2019_06_US_Jinonice/
cover: 00.jpg
img: [
01.jpg,
02.jpg,
11.jpg,
12.jpg,
13.jpg,
14.jpg,
15.jpg]
---