---
title: "Sterboholy"
date: 2018-07-01
Description: "Residence STERBOHOLY"
studio: "EBM Expert"
location: "Prague, Czechia"
scope: "Rezidence STERBOHOLY je rezidenčný projekt nachádzajúci sa na rozhraní priemyslenej zóny a zóny obytnej. Koncept sa na tento kontrast snaží reagovať, hlavne zapadnúť medzi existujucu zástavbu rodinných domov.
Na pokyn investora som v tomto projekte spracovala kompletný redizajn existujúceho dispozičného riešenia bytových jednotiek. Zadaním bolo vytvoriť bytové jednotky na základe manuálu investora pre mladé a rozrastajúce sa rodiny. Spracovala som návrh dispozičného riešenia všetkých bytových a nebytových priestorov a takisto redizajn podzemného podlažia."
year: "2017"
cover: files/2018-07_Sterboholy/EBM_Sterboholy_003_180806-2.jpg
img: [../../files/2018-07_Sterboholy/EBM_Sterboholy_001_180806.jpg,
    ../../files/2018-07_Sterboholy/EBM_Sterboholy_003_180806.jpg,
    ../../files/2018-07_Sterboholy/STERB_01.jpg,
    ../../files/2018-07_Sterboholy/STERB_02.jpg,
    ../../files/2018-07_Sterboholy/STERB_04.jpg
]
---

