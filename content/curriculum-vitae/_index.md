---
title: "Ing. arch. Miroslava Mihalčinová"
date: 2018-12-23T10:51:56+01:00
Description: ""
img: 
---

{{< rawhtml >}}

<table class="ui very basic compact fixed table">
    <tr>
        <td rowspan="19" class="top aligned right aligned">experience:</td>
        <td>2021-present</td>
    </tr>
    <tr><td>Architect</td></tr>
    <tr><td>LostArch</td></tr>
    <tr><td>&nbsp</td></tr>
    <tr><td>2020-present</td></tr>
    <tr><td>Architect</td></tr>
    <tr><td>proarchitech</td></tr>
    <tr><td>&nbsp</td></tr>
    <tr><td>2016-2020</td></tr>
    <tr><td>Architect</td></tr>
    <tr><td>EBM Expert</td></tr>
    <tr><td>&nbsp</td></tr>
    <tr><td>2016</td></tr>
    <tr><td>Junior Architect</td></tr>
    <tr><td>4a Architekti</td></tr>
    <tr><td>&nbsp</td></tr>
    <tr><td>2011-2012</td></tr>
    <tr><td>Visual Merchandiser</td></tr>
    <tr><td>IKEA Czech Republic</td></tr>
    <tr><td>&nbsp</td></tr>
    <tr>
        <td rowspan="12" class="top aligned right aligned">education:</td>
        <td>2012-2018</td>
    </tr>
    <tr><td>Czech Technical University in Prague</td></tr>
    <tr><td>Architecture and Urbanism</td></tr>
    <tr><td>&nbsp</td></tr>
    <tr><td>2011</td></tr>
    <tr><td>Slovak University of Technology in Bratislava</td></tr>
    <tr><td>Architecture and Urbanism</td></tr>
    <tr><td>&nbsp</td></tr>
    <tr><td>2006-2010</td></tr>
    <tr><td>School of Applied Arts, Košice, Slovakia</td></tr>
    <tr><td>Art and Design</td></tr>
    <tr><td>&nbsp</td></tr>
    <tr>
        <td rowspan="12" class="top aligned right aligned">skills:</td>
        <td>AutoCad</td>
    </tr>
    <tr><td>ArchiCad</td></tr>
    <tr><td>Sketchup</td></tr>
    <tr><td>Adobe Photoshop</td></tr>
    <tr><td>Adobe Illustrator</td></tr>
    <tr><td>Adobe InDesign</td></tr>
    <tr><td>Mac OS X</td></tr>
    <tr><td>Microsoft Windows</td></tr>
    <tr><td>Microsoft Word</td></tr>
    <tr><td>Microsoft PowerPoint</td></tr>
    <tr><td>Microsoft Excel</td></tr>
    <tr><td>&nbsp</td></tr>
    <tr>
        <td rowspan="9" class="top aligned right aligned">awards:</td>
        <td>2010</td>
    </tr>
    <tr><td>1st place</td></tr>
    <tr><td>Redesign of the school's graphic portfolio for its 40th anniversary</td></tr>
    <tr><td><a href="http://www.suvke.sk" target="_blank">www.suvke.sk</a></td></tr>
    <tr><td>&nbsp</td></tr>
    <tr><td>2008</td></tr>
    <tr><td>1st place</td></tr>
    <tr><td>Model Young Package</td></tr>
    <tr><td><a href="https://young-package.com/historie/2008-2/gallery-of-the-winners/" target="_blank">www.young-package.com</a></td></tr>
</table>

{{< /rawhtml >}}
