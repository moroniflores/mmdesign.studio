$(document).ready(function(){
    $("#lightgallery").lightGallery({
    	thumbnail:true,
    	share:false,
    	zoom:false,
    	rotate: false,
    	fullScreen:false,
    	autoplayControls:false,
    });
});